import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import * as interceptor from './interceptor.js';
import API from '../constants/api.js';

axios.interceptors.request.use(interceptor.request);
axios.interceptors.response.use(interceptor.response, interceptor.responseError);

Vue.use(VueAxios, axios)

export default {
	currencyPairs () {
		return axios.get(`${API.API_URL}/instrument/active`);
	},
	currencyPair (params) {
		return axios({
			method: 'get',
			url: `${API.API_URL}/trade/bucketed`,
			params: params,
		});
	},
	currencyOrder (data) {
		return axios({
			method: 'post',
			url: `${API.API_URL}/order`,
			data: data,
		});
	},
	currencyOrderGet (params) {
		return axios({
			method: 'get',
			url: `${API.API_URL}/order`,
			params: params,
		});
	},
};

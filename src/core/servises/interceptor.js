import API from '../constants/api.js';
import APP from '../constants/app.js';
import * as CryptoJS from 'crypto';

export const request = config => {
	const { headers, method, url, params, data } = config;
	if(url.search(APP.ORDER) !== -1) {
		const verb = method.toUpperCase();
		const path = '/api/v1/order';
		let parameters = new URLSearchParams();
		for(let key in params) {
			parameters.append(key, params[key]);
		}
		parameters = parameters.toString() ? '?' + parameters.toString() : '';
		const expires = Math.round(new Date().getTime() / 1000) + 60;
		const postBody = data ? JSON.stringify(data) : '';
		const signature = CryptoJS.createHmac('sha256', API.API_SECRET).update(verb + path + parameters + expires + postBody).digest('hex');
		const orderHeaders = {
			'content-type' : 'application/json',
			'Accept': 'application/json',
			'X-Requested-With': 'XMLHttpRequest',
			'api-expires': expires,
			'api-key': API.API_KEY,
			'api-signature': signature
		};
		config.headers = {...headers, ...orderHeaders}
	}
	return config;
};

export const response = res => {
	return res;
};

export const responseError = rejection => {
	return Promise.reject(rejection);
};
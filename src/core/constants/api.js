export default {
  API_KEY: process.env.VUE_APP_API_KEY,
  API_SECRET: process.env.VUE_APP_API_SECRET,
  API_URL: 'https://testnet.bitmex.com/api/v1',
}